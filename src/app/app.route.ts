import { Routes } from "@angular/router";
import { InfoComponent } from './info/info.component';


const route : Routes = [

    {
        path : 'info',
        component : InfoComponent 
    }

];

export route;