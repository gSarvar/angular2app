import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { InfoComponent } from './info/info.component';
import { HomeComponent } from './home/home.component';
import { RouterModule,Routes } from '@angular/router';
import {Route} from './app.route';

@NgModule({
  declarations: [
    AppComponent,
    InfoComponent,
    HomeComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    RouterModule.forRoot(Route.route)
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
